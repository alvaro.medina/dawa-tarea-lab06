const express = require("express");
const app = express();
const pug = require('pug');
app.use(express.json());
app.use(express.urlencoded());

const port = process.env.PORT || 3000

app.use(express.static(__dirname + '/public'))



app.set('view engine', 'pug');

app.get("/", function (req, res) {
    res.render('index', {

    })
});
app.get("/about", function (req, res) {
    res.render('about', {

    })
});
app.get("/property-grid", function (req, res) {
    res.render('property-grid', {

    })
});
app.get("/blog", function (req, res) {
    res.render('blog', {

    })
});
app.get("/contact", function (req, res) {
    res.render('contact', {

    })
});
app.post("/request", function (req, res) {
    var name = req.body.name
    var email = req.body.email
    var date = req.body.date
    var telf = req.body.telf
    var message = req.body.message
    res.render('request', {
        name: name,
        email: email,
        date: date,
        telf: telf,
        message: message,
    })
});
app.listen(port, () => console.log(`escuchando peticiones en el puerto ${port}`));